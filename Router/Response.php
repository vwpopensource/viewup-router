<?php

namespace Router;

/**
 *
 */
class Response {

	public $app;

	function __construct( App $app ) {
		$this->app = $app;
	}

	public function write( $ouput ) {
		if ( is_array( $ouput ) ) {
			echo '<pre>';
			print_r( $ouput );
		} else {
			echo $ouput;
		}
	}

	public function json( $json, $inJSON = false ) {
		$this->withHeader( 'Content-type', 'application/json' )
		     ->withHeader( 'Cache-Control', 'no-cache;must-revalidate' );
		echo( $inJSON ? $json : json_encode( $json ) );
	}

	public function withHeader( $headerName, $headerValue ) {
		header( $headerName . ': ' . $headerValue );

		return $this;
	}

	/**
	 * @param int $statusCode http status code, like 200,404,501
	 *
	 * @deprecated use status()
	 */
	public function withStatus( $statusCode = 200 ) {
		http_response_code( $statusCode );

		return $this;
	}

	/**
	 * @param int $statusCode http status code, like 200,404,501
	 *
	 * @return  Response
	 */
	public function status( $status ) {
		http_response_code( $status );

		return $this;
	}

	/**
	 * @param $template
	 * @param $data
	 */
	public function render( $template, $data ) {
		$this->app->config->templateEngine->render( $template, $data );
	}

	public function mailTemplate( $template, $data ) {
		return $this->app->config->templateEngine->mailTemplate( $template, $data );
	}

	public function customView( $view, $data = array() ) {
		$params = $data;
		unset( $data );
		include $view;
	}
}
