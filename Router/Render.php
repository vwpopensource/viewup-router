<?

namespace Router;

use Twig_Environment;
use Twig_Extension_Debug;
use Twig_Filter;
use Twig_Loader_Filesystem;
use Twig_SimpleFilter;
use  Twig_Function;

Class Render {

	/**
	 * Render constructor.
	 *
	 * @param string $viewPath
	 * @param string $viewCache
	 */
	private $renderFile;
	private $cachePath;
	private $filters = [];
	private $functions = [];

	public function __construct( $viewPath, $debug, $viewCache = false ) {
		$loader          = new Twig_Loader_Filesystem( $viewPath );
		$this->cachePath = $viewCache;
		$twigConfig      = [];
		if ( $this->cachePath ) {
			$twigConfig['cache'] = $this->cachePath;
		}
		$twigConfig['debug'] = $debug;
		$this->renderFile    = new Twig_Environment( $loader, $twigConfig );

		$this->enableDump();
	}

	public function addTwigFilter( $filterName, $callback, $option = [] ) {
		$this->filters[] = new Twig_Filter( $filterName, $callback, $option );
		$this->updateFilter();
	}

	public function addTwigFunction( $functionName, $callback, $option = [] ) {
		$this->functions[] = new Twig_Function( $functionName, $callback, $option );
		$this->updateFunctions();
	}

	public function updateFilter() {
		foreach ( $this->filters as $filter ) {
			$this->renderFile->addFilter( $filter );
		}
	}

	public function updateFunctions() {
		foreach ( $this->functions as $function ) {
			$this->renderFile->addFunction( $function );
		}
	}

	public function render( $fileName, $data = [] ) {

		if ( count( $data ) > 0 ) {
			echo $this->renderFile->render( $fileName, $data );
		} else {
			echo $this->renderFile->render( $fileName );
		}
	}

	public function enableDump() {
		$this->renderFile->addExtension( new Twig_Extension_Debug() );
	}

	public function mailTemplate( $template, $data ) {
		if ( count( $data ) > 0 ) {
			return $this->renderFile->render( $template, $data );
		} else {
			return $this->renderFile->render( $template );
		}
	}
}