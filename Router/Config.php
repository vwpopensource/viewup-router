<?php

namespace Router;

/**
 *	Router configuration
 *	multiples routers can use the same configuration instance
 */
class Config {

	/**
	 * @var Render $templateEngine
	 */

	public $templateEngine;
	private $instance;

	function __construct() {}

	/**
	 * @return mixed
	 */
	public function getInstance() {
		return $this->instance;
	}

	/**
	 * @param mixed $instance
	 */
	public function setInstance($instance) {
		$this->instance = $instance;
		if (!$this->templateEngine) {
			$this->templateEngine = new Render(
				$this->getInstance()->templatePath,
				$this->getInstance()->twigDebug,
				$this->getInstance()->templateCache
			);
		}
	}
}
